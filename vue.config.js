module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/~cs62160140/learn_bootstrap/'
      : '/'
}
